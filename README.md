# README #

This program is Catch.com Code Challenge written in PHP with the help of Symfony Libraries.
This program will take data from jsonl files from https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl and will output to csv file by default.

### Requirements ###

* Install [PHP Version 7.x](https://www.php.net/downloads.php) and add the PHP bin path to the classpath of your operating system
* Install [Composer](https://getcomposer.org/)
* Change settings in php.ini. Ensure that this particular settings is applied`allow_url_fopen=On` 

### Pre-requisities to run the code ###

* Open shell window and go to the main folder of Catch.com Code Challenge
* Run below command:
```sh
composer update
```
* Ensure every file in bin folder (where phpunit, console and catch-code-challenge file resides has executable permission) by run below command
```sh
chmod 755 bin/*
```
### How to run the program ###

* Open shell window and go to the main folder of Catch.com Code Challenge
* Run below command:
```sh
bin/catch-code-challenge run
```
* Add an argument `--email` to send the output to the mentioned email, for example
```sh
bin/catch-code-challenge run --email ganda.manurung@gmail.com
```
* To export in other format, add an argument `--output_format` with following options format: `csv`, `yaml`, `xml`, and `jsonl`. Default format is `csv` when the option is not given.
```sh
bin/catch-code-challenge run --output_format yaml
```

### How to run the tests ###

* Open shell window and go to the main folder of Catch.com Code Challenge
* Run below command:
```sh
bin/phpunit tests/
```