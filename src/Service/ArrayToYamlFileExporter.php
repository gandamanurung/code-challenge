<?php

namespace GandaManurung\Service;

use Symfony\Component\Yaml\Yaml;

class ArrayToYamlFileExporter extends ArrayToFileExporter
{
	
	/**
     * export to yaml file
     *
     * @return true if everything is work fine
     */

    public function export($pathToFile)
    {
    	$yaml = Yaml::dump($this->dataArray);
        file_put_contents($pathToFile, $yaml);
        return true;
    }
}