<?php

namespace GandaManurung\Service;

use Rs\JsonLines\JsonLines;

class ArrayToJsonlFileExporter extends ArrayToFileExporter
{
	
	/**
     * export to jsonl file
     *
     * @return true if everything is work fine
     */

    public function export($pathToFile)
    {
       (new JsonLines())->enlineToFile($this->dataArray['Orders'], $pathToFile);
        return true;
    }
}