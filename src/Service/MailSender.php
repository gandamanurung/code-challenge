<?php

namespace GandaManurung\Service;

use Symfony\Component\Filesystem\Path;
use Symfony\Component\Dotenv\Dotenv;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MailSender
{

    private const APP_ROOT_PATH = __DIR__ . '/../../';
    
    public static function sendEmail($emailSendTo, $emailSendFrom, $emailSendFromName, $emailSubject, $emailBody, $attachments)
    {
        $dotenv = new Dotenv();
        $dotenv->load( self::APP_ROOT_PATH.'/.env');

        $mail = new PHPMailer(true);

        $mail->isSMTP();
        $mail->Host       = $_ENV['PHPMAILER_SMTP_HOST'];
        $mail->SMTPAuth   = true;
        $mail->Username   = $_ENV['PHPMAILER_SMTP_USERNAME'];
        $mail->Password   = $_ENV['PHPMAILER_SMTP_PASSWORD'];
        $mail->SMTPSecure = 'tls';
        $mail->Port       = $_ENV['PHPMAILER_SMTP_PORT'];

        $mail->setFrom($emailSendFrom, $emailSendFromName);
        $mail->addAddress($emailSendTo);


        foreach ($attachments as $attachment) {
            $mail->addAttachment($attachment);
        }

        $mail->isHTML(true);
        $mail->Subject = $emailSubject;
        $mail->Body    = $emailBody;
        $mail->AltBody = $emailBody;

        return $mail->send();
    }
}