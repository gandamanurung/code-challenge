<?php

namespace GandaManurung\Service;

use GandaManurung\AppBundle\Utility\FileDownloadUtility;
use GandaManurung\AppBundle\Model\OrderSummaryModel;
use GandaManurung\AppBundle\Utility\FileNameManagementUtility;
use GandaManurung\AppBundle\Utility\LinterUtility;


use Symfony\Component\Filesystem\Path;

class CatchChallengeCommandExecutor
{

    private const APP_ROOT_PATH = __DIR__ . '/../../';

    /**
     * sets of configuration to define set of parameters in order for this class to be run correctly
     *
     * @var array
     */
    protected $config = [
        'JSONL_URL' => 'https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl',
        'DOWNLOAD_SAVE_PATH' => self::APP_ROOT_PATH. '/download/',
        'OUTPUT_SAVE_PATH' =>  self::APP_ROOT_PATH . '/out/',
        ''
    ];


    /**
     * Execute the command
     *
     * @param String $emailSendTo
     * @param String $outputFormat
     * @return String Return saved file path when success
     * @throws Exception if output format is csv and csv not valid
     */
    public function execute($emailSendTo, $outputFormat = 'csv')
    {
        $outputFilePath = 'C:\xampp7\htdocs\code-challenge\out\out_1649321174.csv';

        $downloadedFilePath = FileDownloadUtility::downloadFile($this->config['JSONL_URL'], $this->config['DOWNLOAD_SAVE_PATH']);

        $outputFolderPath = Path::canonicalize($this->config['OUTPUT_SAVE_PATH']);

        $orderArray = explode("\n", file_get_contents($downloadedFilePath));

        $csvHeaders = ['Order ID', 'Order Date Time', 'Total Order Value', 'Average Unit Price', 'District Unit Count', 'Total Unit Count', 'Customer State', 'Customer Latitude', 'Customer Longitude'];

        if (empty($outputFormat)) {
            $csvRecord['Orders'][] = $csvHeaders;
            $outputFormat = 'csv';
        }

        foreach ( $orderArray as $orderLine )
        {
            $jsonObject = json_decode($orderLine);

            if ( !empty ($jsonObject) )
            {
                $orderSummary = new OrderSummaryModel($jsonObject);

                if($orderSummary->getTotalOrderValue() > 0) {
                    $csvRecord['Orders'][] = [
                                'OrderID' => $orderSummary->getOrderId(),
                                'OrderDate' => $orderSummary->getOrderDate(),
                                'TotalOrderValue' => $orderSummary->getTotalOrderValue(),
                                'AverageUnitPrice' => $orderSummary->getAverageUnitPrice(),
                                'DistinctUnitCount' => $orderSummary->getDistinctUnitCount(),
                                'TotalUnitCount' => $orderSummary->getTotalUnitCount(),
                                'CustomerState' => $orderSummary->getCustomerState(),
                                'CustomerGeoLat' => $orderSummary->getCustomerGeoLat(),
                                'CustomerGeoLng' => $orderSummary->getCustomerGeoLng()
                            ];
                }

            }
        }

        $outputFormat = empty($outputFormat)? 'csv' : $outputFormat;

        $exporterClassName = sprintf('GandaManurung\Service\ArrayTo%sFileExporter', ucwords($outputFormat));

        $ref = new \ReflectionClass($exporterClassName);
        $exporter = $ref->newInstanceArgs(array($csvRecord));

        // $exporter = new ArrayToCsvFileExporter($csvRecord);

        $outputFilePath = $outputFolderPath . DIRECTORY_SEPARATOR .  FileNameManagementUtility::generateNewFileName('out', $outputFormat, true);
        $exporter->export($outputFilePath);

        if ($outputFormat === 'csv' && !LinterUtility::validCsv($outputFilePath)) {
            throw new \Exception("The exported csv file is not a valid csv");
        }

        if (null !== $emailSendTo) {
            MailSender::sendEmail($emailSendTo, 'noreply@gandamanurung.com', 'NoReply', 'Order JSON file exported succesfully', 'Please find the exported file in the attachment', [$outputFilePath]);
        }

        return $outputFilePath;
    }
}