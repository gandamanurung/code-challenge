<?php

namespace GandaManurung\Service;

interface IArrayFileExport
{
	
	/**
     * export to file
     *
     * @return generated file name
     */
	public function export($pathToFile);

}