<?php

namespace GandaManurung\Service;


abstract class ArrayToFileExporter implements IArrayFileExport
{

    /**
     * data array of file that need to be exported
     *
     * @var array
     */
	
	protected $dataArray;

	/**
     * construct new object of ArrayToFileExporter
     *
     * @param  Array $dataArray data that needs to be exported
     */
	public function __construct($dataArray)
    {
        $this->dataArray = $dataArray;
    }

}