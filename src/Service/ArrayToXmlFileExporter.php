<?php

namespace GandaManurung\Service;

use Spatie\ArrayToXml\ArrayToXml;

class ArrayToXmlFileExporter extends ArrayToFileExporter
{
	
	/**
     * export to xml file
     *
     * @return true if everything is work fine
     */

    public function export($pathToFile)
    {
        $result = ArrayToXml::convert($this->dataArray);
        file_put_contents($pathToFile, $result);
        return true;
    }
}