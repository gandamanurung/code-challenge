<?php

namespace GandaManurung\Service;

use League\Csv\Writer;
use League\Csv\CannotInsertRecord;

class ArrayToCsvFileExporter extends ArrayToFileExporter
{
	
	/**
     * export to csv file
     *
     * @return true if everything is work fine
     */

    public function export($pathToFile)
    {
        try {
    		$csv = Writer::createFromPath($pathToFile, "w+");

    		$csv->insertAll($this->dataArray['Orders']);
            return true;
        } catch (CannotInsertRecord $e) {
            throw $e;
        }
    }
}