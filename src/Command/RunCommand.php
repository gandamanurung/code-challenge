<?php

namespace GandaManurung\CatchCodeChallenge\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use GandaManurung\Service\CatchChallengeCommandExecutor;
use GandaManurung\AppBundle\Utility\LinterUtility;

class RunCommand extends Command
{
    /**
     * The name of the command (the part after "bin/catch-code-challenge").
     *
     * @var string
     */
    protected static $defaultName = 'run';

    /**
     * The command description shown when running "php bin/catch-code-challenge list".
     *
     * @var string
     */
    protected static $defaultDescription = 'Run catch code challenge';

    /**
     * Configuration of command "php bin/catch-code-challenge" with email as input option.
     *
     */
    protected function configure()
    {
        $this->setName('run')
            ->setDescription('Download JSONL file from https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl and process as csv file')
            ->setHelp('Demonstration of custom commands created by Symfony Console component.')
            ->addOption(
                'email',
                null,
                InputOption::VALUE_OPTIONAL,
                'Optional. Provide a valid email address where the result would be sent to',
                null
            )
            ->addOption(
                'output_format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Optional. Choose between csv, yaml, jsonl, or xml. Default is csv',
                null
            );
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int 0 if everything went fine, or an exit code.
     */

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('processing...');

        $commandExecutor = new CatchChallengeCommandExecutor();
        $outputFilePath = $commandExecutor->execute($input->getOption('email'), $input->getOption('output_format'));

        $output->writeln('The processed file is exported to: ' . $outputFilePath);
        
        return Command::SUCCESS;
    }
    
}