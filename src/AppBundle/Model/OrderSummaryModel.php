<?php

namespace GandaManurung\AppBundle\Model;

use GandaManurung\AppBundle\Utility\OrderJSONUtility;

class OrderSummaryModel
{
	
    /**
     * The ID of an order
     *
     * @var string
     */

    protected $orderId;

    /**
     * The date of the order
     *
     * @var string
     */
    protected $orderDate;

    /**
     * The total order including discount but exclude shipping
     *
     * @var double
     */
    protected $totalOrderValue;

    /**
     * Average of total unit price calculated from totalOrderValue
     *
     * @var double
     */
    protected $averageUnitPrice;

    /**
     * Number of disctincts item in the order
     *
     * @var int
     */
    protected $distinctUnitCount;

    /**
     * The total of unit items in the order
     *
     * @var int
     */
    protected $totalUnitCount;

    /**
     * The state of customer shipping address where the order would ship to
     *
     * @var string
     */
    protected $customerState;

    /**
     * The customer address latitude
     *
     * @var string
     */
    protected $customerGeoLatitude;

    /**
     * The customer address longitude
     *
     * @var string
     */
    protected $customerGeoLongitude;

    /**
     * construct a new Order Summary Model given $orderJSON, assuming $orderJSON is always a valid order
     *
     * @param  Object $orderJSON JSON Object of an order
     */

    public function __construct($orderJSON) {

        $this->orderId = $orderJSON->order_id;
        $this->orderDate = OrderJSONUtility::getOrderDateInISO8601Format($orderJSON);
        
        $this->processOtherSummaryFields($orderJSON);

    }

    protected function processOtherSummaryFields($orderJSON) {

        $this->totalOrderValue = 0;
        $this->totalUnitCount = 0;
        $this->distinctUnitCount = count($orderJSON->items);
        $this->customerState = $orderJSON->customer->shipping_address->state;

        $compiledAddress = urlencode($orderJSON->customer->shipping_address->postcode) . ',' .
                        urlencode($orderJSON->customer->shipping_address->street) . ',' .
                        urlencode($orderJSON->customer->shipping_address->suburb) . ',' .
                        urlencode($orderJSON->customer->shipping_address->state);

        $geolocationGoogleResponse = file_get_contents(sprintf("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyCDFD3edzTfahsCFf0o3irKzI-CRphHIsc", $compiledAddress));

        $geolocationGoogleJsonObject = json_decode($geolocationGoogleResponse);

        if(sizeof($geolocationGoogleJsonObject->results) > 0) {
            $this->customerGeoLatitude = $geolocationGoogleJsonObject->results[0]->geometry->location->lat;
            $this->customerGeoLongitude = $geolocationGoogleJsonObject->results[0]->geometry->location->lng;
        }

        foreach ($orderJSON->items as $orderItemIndex => $orderItem) {

            $orderItemTotal = $orderItem->unit_price * $orderItem->quantity;
            $this->totalOrderValue += $orderItemTotal;
            $this->totalUnitCount += $orderItem->quantity;

            if( !empty($orderJSON->discounts) && array_key_exists($orderItemIndex, $orderJSON->discounts) ) {

                if ($orderJSON->discounts[$orderItemIndex]->type === 'DOLLAR') {
                    $discount = $orderJSON->discounts[$orderItemIndex]->value * $orderItem->quantity;
                }
                else {
                    // percentage discount
                    $discount = ($orderJSON->discounts[$orderItemIndex]->value / 100) * $this->totalOrderValue;
                }

                $this->totalOrderValue -= $discount;

            }

        }

        $this->averageUnitPrice = $this->totalOrderValue / $this->totalUnitCount;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function getOrderDate() {
        return $this->orderDate;
    }

    public function getTotalOrderValue() {
        return $this->totalOrderValue;
    }

    public function getAverageUnitPrice() {
        return $this->averageUnitPrice;
    }

    public function getDistinctUnitCount() {
        return $this->distinctUnitCount;
    }

    public function getTotalUnitCount() {
        return $this->totalUnitCount;
    }

    public function getCustomerState() {
        return $this->customerState;
    }

    public function getCustomerGeoLat() {
        return $this->customerGeoLatitude;
    }

    public function getCustomerGeoLng() {
        return $this->customerGeoLongitude;
    }
}