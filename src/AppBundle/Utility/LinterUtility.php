<?php

namespace GandaManurung\AppBundle\Utility;

use Symfony\Component\HttpClient\CurlHttpClient;

use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

class LinterUtility
{
	/**
     * Validate a csv against csvlint.io
     *
     * @param  String $csvFilePath
     * @return Boolean true is csv is a valid csv
     */

	public static function validCsv($csvFilePath) 
	{

		$client = new CurlHttpClient();

		$httpRequestHeaders = [
								'Authorization' => 'Bearer public_kW15aqZBK6ph2W691sitDmtRJuQ6',
								'Content-Type' => 'text/csv'
								];

		$responseUpload = $client->request('POST', 'https://api.upload.io/v1/files/basic', [
					    'headers' => $httpRequestHeaders,
					    'body' => file_get_contents($csvFilePath)
					]);

		if($responseUpload->getStatusCode() === 200) {

			$jsonContentUpload = json_decode($responseUpload->getContent());
			
			$responseCsvValidate = $client->request('POST', 'http://csvlint.io/package.json', [
					    'body' => 'urls[]='.$jsonContentUpload->fileUrl
					]);

			if($responseCsvValidate->getStatusCode() === 200) {

				$jsonContentValidate = json_decode($responseCsvValidate->getContent());


				$jsonLinterResultUrl = $jsonContentValidate->package->url . '.json';

				$linterResultOutput = json_decode(file_get_contents($jsonLinterResultUrl));

				if($linterResultOutput->package->validations[0]->state !== 'invalid') {
					return true;
				}

				return false;
			}

			return false;
		}

		return false;
    }
}