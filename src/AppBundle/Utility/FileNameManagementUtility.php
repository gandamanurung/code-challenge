<?php

namespace GandaManurung\AppBundle\Utility;

use Symfony\Component\Filesystem\Filesystem;

class FileNameManagementUtility
{
	
    /**
     * Add unix timestamp to a file name, for example ganda.txt will become ganda_1647883201.txt
     *
     * @param  String $baseFileName
     * @return String new file name
     * @throws Exception when given $baseFileName or $newExtension is empty or null
     */

    public static function addUnixTimestampToFileName($baseFileName) {
        
        $baseFileName = trim($baseFileName);

        if ( empty($baseFileName) ){
            throw new \Exception("BaseFileName could not be null nor empty");
        }

        $pathInformation = pathinfo($baseFileName);
        return $pathInformation['filename'] . '_' . time() . '.' . $pathInformation['extension'];
    }

    /**
     * Generate a new file path given prefix and extension. Formula: {prefix}{dot}{extension} 
     * When $withTimestampInTheFileName given true, the generated file name will have this following formula:
     * {prefix}{underscore}{unixTimestamp}{dot}{extension} 
     *
     * @param  String $prefix, a prefix of a file name
     * @param  String $extension, extension of a file
     * @param  Boolean $withTimestampInTheFileName, to have unixTimestamp suffix in the file
     * @return String new file name
     */

    public static function generateNewFileName($prefix = 'out', $extension = 'csv', $withTimestampInTheFileName = false) {
        return $prefix  . ($withTimestampInTheFileName?  '_' . time() : '') . '.' . $extension;
    }


    /**
     * Rename extension of a file, for example ganda.txt could be renamed into ganda.csv
     *
     * @param  String $baseFileName
     * @param  String $newExtension
     * @return String a new file name with a replaced extension
     * @throws Exception when given $baseFileName or $newExtension is empty or null
     */

    public static function changeFileNameExtension($baseFileName, $newExtension) {

        $baseFileName = trim($baseFileName);
        $newExtension = trim($newExtension);


        if ( empty($baseFileName) || empty($newExtension)){
            throw new \Exception("BaseFileName and the new extension could not be null nor empty");
        }

        $pathInformation = pathinfo($baseFileName);
        return $pathInformation['filename'] . '.' . $newExtension;
    }
}