<?php

namespace GandaManurung\AppBundle\Utility;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\HttpClient\HttpClient;

class FileDownloadUtility
{
	/**
     * Download a file and save to $saveFolderPath
     *
     * @param  String $downloadFromUrl
     * @param  String $saveFolderPath
     * @return String absolute path to the downloaded file
     * @throws Exception if path to download folder is not existed or file download failed
     */

	public static function downloadFile($downloadFromUrl, $saveFolderPath) 
	{

		$client = HttpClient::create();

		$saveFolderPath = Path::canonicalize($saveFolderPath);
		
		$fileSystem = new Filesystem();

		if (!$fileSystem->exists($saveFolderPath)) {
			throw new \Exception("The path " . $saveFolderPath . " is not existed in the file system");
		}

		$response = $client->request('GET', $downloadFromUrl);

		$statusCode = $response->getStatusCode();

		if($statusCode === 200) {

			$fileName = basename($downloadFromUrl);
			$content = $response->getContent();
			$newFileName = FileNameManagementUtility::addUnixTimestampToFileName($fileName);
			$newFilePath = $saveFolderPath . DIRECTORY_SEPARATOR . $newFileName;

			try {

				$fileSystem->appendToFile($newFilePath, $content);
				return $newFilePath;

			} 
			catch(\Exception $e) {
			    throw $e;
			}

		}
		else {
			throw new Exception("Unable to download the file. The server  returned with HTTP error code " . $statusCode);
		}

    }
}