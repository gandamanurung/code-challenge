<?php

namespace GandaManurung\AppBundle\Utility;

use Symfony\Component\Filesystem\Filesystem;

class OrderJSONUtility
{
	
    /**
     * Convert Order Date in ISO 8601 format in UTC Timezone
     *
     * @param  Object $orderJSON JSON Object of an order
     * @return double total order 
     */

    public static function getOrderDateInISO8601Format($orderJSON) {
        $timeZone = new \DateTimeZone('UTC');
        $dateTime = new \DateTime($orderJSON->order_date, $timeZone);
        return $dateTime->format('c');
    }

}