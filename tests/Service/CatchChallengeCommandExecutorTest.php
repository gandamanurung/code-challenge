<?php

namespace GandaManurung\Tests\Service;

use GandaManurung\Service\CatchChallengeCommandExecutor;

use PHPUnit\Framework\TestCase;

class CatchChallengeCommandExecutorTest extends TestCase
{
	
	public function testCatchChallengeCommandExecutor(): void
    {
        $executor = new CatchChallengeCommandExecutor();
        $savedFilePath = $executor->execute('ganda.manurung@gmail.com');
        $this->assertFileExists($savedFilePath);
    }
}