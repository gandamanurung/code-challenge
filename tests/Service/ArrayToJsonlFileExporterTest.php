<?php

namespace GandaManurung\Tests\Service;

use GandaManurung\Service\ArrayToJsonlFileExporter;

use PHPUnit\Framework\TestCase;

class ArrayToJsonlFileExporterTest extends TestCase
{
	
	public function testArrayToJsonlFileExporter(): void
    {

        $csvRecord['Orders'][] = ['OrderId' => '1001', 'OrderDate' => '2019-03-08T12:13:29+00:00', 'TotalOrderValue' => '359.78', 'AverageUnitPrice' => '59.963333333333', 'DistinctUnitCount' => '2', 'TotalUnitCount' => '6', 'CustomerState' => 'VICTORIA'];
        $csvRecord['Orders'][] = ['OrderId' => '1002', 'OrderDate' => '2019-03-08T12:13:29+00:00', 'TotalOrderValue' => '359.78', 'AverageUnitPrice' => '59.963333333333', 'DistinctUnitCount' => '2', 'TotalUnitCount' => '6', 'CustomerState' => 'VICTORIA'];

        $exporter = new ArrayToJsonlFileExporter($csvRecord);
        $this->assertInstanceOf(ArrayToJsonlFileExporter::class, $exporter);
        $this->assertTrue($exporter->export('out'.time().'.jsonl'));
    }
}