<?php

namespace GandaManurung\Tests\Service;

use GandaManurung\Service\ArrayToYamlFileExporter;

use PHPUnit\Framework\TestCase;

class ArrayToYamlFileExporterTest extends TestCase
{
	
	public function testArrayToYamlFileExporter(): void
    {

        $csvRecord['Orders'][] = ['OrderId' => '1001', 'OrderDate' => '2019-03-08T12:13:29+00:00', 'TotalOrderValue' => '359.78', 'AverageUnitPrice' => '59.963333333333', 'DistinctUnitCount' => '2', 'TotalUnitCount' => '6', 'CustomerState' => 'VICTORIA'];

        $exporter = new ArrayToYamlFileExporter($csvRecord);
        $this->assertInstanceOf(ArrayToYamlFileExporter::class, $exporter);
        $this->assertTrue($exporter->export('out'.time().'.yaml'));
    }
}