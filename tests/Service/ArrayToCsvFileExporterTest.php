<?php

namespace GandaManurung\Tests\Service;

use GandaManurung\Service\ArrayToCsvFileExporter;

use PHPUnit\Framework\TestCase;

class ArrayToCsvFileExporterTest extends TestCase
{
	
	public function testArrayToCsvFileExporter(): void
    {
        $csvHeaders = ['Order ID', 'Order Date Time', 'Total Order Value', 'Average Unit Price', 'District Unit Count', 'Total Unit Count', 'Customer State'];

        $csvRecord['Orders'][] = $csvHeaders;
        $csvRecord['Orders'][] = ['OrderId' => '1001', 'OrderDate' => '2019-03-08T12:13:29+00:00', 'TotalOrderValue' => '359.78', 'AverageUnitPrice' => '59.963333333333', 'DistinctUnitCount' => '2', 'TotalUnitCount' => '6', 'CustomerState' => 'VICTORIA'];

        $exporter = new ArrayToCsvFileExporter($csvRecord);
        $this->assertInstanceOf(ArrayToCsvFileExporter::class, $exporter);
        $this->assertTrue($exporter->export('out'.time().'.csv'));
    }
}