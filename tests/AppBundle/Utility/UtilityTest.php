<?php

namespace GandaManurung\Tests\AppBundle\Utility;

use GandaManurung\AppBundle\Utility as Util;

use PHPUnit\Framework\TestCase;

class UtilityTest extends TestCase
{
	
	public function testUtility(): void
    {

        $csvFileNameWithoutTimestamp =  Util\FileNameManagementUtility::generateNewFileName('out', 'csv', false);
        $csvFileNameWithTimestamp =  Util\FileNameManagementUtility::generateNewFileName('out', 'csv', true);
        $yamlFileNameWithoutTimestamp =  Util\FileNameManagementUtility::generateNewFileName('out', 'yaml', false);
        $yamlFileNameWithTimestamp =  Util\FileNameManagementUtility::generateNewFileName('out', 'yaml', true);

        $this->assertStringEndsWith('.csv', $csvFileNameWithoutTimestamp);
        $this->assertStringEndsWith('.csv', $csvFileNameWithTimestamp);
        
        $this->assertStringEndsWith('.yaml', $yamlFileNameWithoutTimestamp);
        $this->assertStringEndsWith('.yaml', $yamlFileNameWithTimestamp);

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}\.csv/', 
            $csvFileNameWithoutTimestamp
        );

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}_{1}\d{1,}\.csv/', 
            $csvFileNameWithTimestamp
        );

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}\.yaml/', 
            $yamlFileNameWithoutTimestamp
        );

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}_{1}\d{1,}\.yaml/',  
            $yamlFileNameWithTimestamp
        );

        $newCsvFileNameWithTimestamp =  Util\FileNameManagementUtility::addUnixTimestampToFileName($csvFileNameWithoutTimestamp);
        $newYamlFileNameWithTimestamp =  Util\FileNameManagementUtility::addUnixTimestampToFileName($yamlFileNameWithoutTimestamp);

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}_{1}\d{1,}\.csv/', 
            $newCsvFileNameWithTimestamp
        );

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}_{1}\d{1,}\.yaml/', 
            $newYamlFileNameWithTimestamp
        );

        $newJsonlFileNameWithTimestamp =  Util\FileNameManagementUtility::changeFileNameExtension($newCsvFileNameWithTimestamp, 'jsonl');
        $newXMLFileNameWithTimestamp =  Util\FileNameManagementUtility::changeFileNameExtension($newYamlFileNameWithTimestamp, 'xml');

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}_{1}\d{1,}\.jsonl/', 
            $newJsonlFileNameWithTimestamp
        );

        $this->assertMatchesRegularExpression(
            '/[a-z]{3}_{1}\d{1,}\.xml/', 
            $newXMLFileNameWithTimestamp
        );

        $jsonContent = file_get_contents(__DIR__ . '/../Model/order.json');
        $jsonOrder   = json_decode($jsonContent);
        $orderDate = Util\OrderJSONUtility::getOrderDateInISO8601Format($jsonOrder);
        $this->assertSame('2019-03-12T07:56:40+00:00', $orderDate);

        $downloadedFilePath = Util\FileDownloadUtility::downloadFile('https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl', __DIR__ . '/../../../download/');

        $this->assertFileExists($downloadedFilePath);

    }
}