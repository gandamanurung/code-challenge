<?php

namespace GandaManurung\Tests\AppBundle\Model;

use GandaManurung\AppBundle\Model\OrderSummaryModel;

use PHPUnit\Framework\TestCase;

class OrderSummaryModelTest extends TestCase
{
	
	public function testOrderSummaryModel(): void
    {
        $jsonContent = file_get_contents(__DIR__ . '/order.json');
        $jsonOrder   = json_decode($jsonContent);

        $orderSummaryModel = new OrderSummaryModel($jsonOrder);

        $this->assertSame(1068, $orderSummaryModel->getOrderId());
        $this->assertSame('2019-03-12T07:56:40+00:00', $orderSummaryModel->getOrderDate());
        $this->assertSame(526.92, $orderSummaryModel->getTotalOrderValue());
        $this->assertSame(47.90181818181818, $orderSummaryModel->getAverageUnitPrice());
        $this->assertSame(7, $orderSummaryModel->getDistinctUnitCount());
        $this->assertSame(11, $orderSummaryModel->getTotalUnitCount());
        $this->assertSame('NEW SOUTH WALES', $orderSummaryModel->getCustomerState());


    }
}